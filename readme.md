
##  Tower inspection with Rotors simulator

In order to execute the missions defined in configs/mission, perform the following steps:

- Execute:

	$ roscore

- Execute the script that launches Gazebo and the Aerostack components for this project:

        $ ./main_launcher.sh


The operator ha to select the "floor_X_error" in case that the floor looks like this:
In any other case, the answer must be "floor_X_ok".



In the folder /tower_inspection_rotors_simulator/rotors_files/model/Tower/meshes you can find the tower_3.dae and a folder named "Stages". 
"Stages" contains other scenarios examples besides Tower_3.dae. If you want to try other stage do the following steps:

- Remove tower_3.dae

- Copy and paste de .dae you want to try

- Rename it with "tower_3.dae"

- Execute the script that launches Gazebo and the Aerostack components for this project:

        $ ./main_launcher.sh



Here is a image of the stage

![gazebo_launcher.png](https://i.ibb.co/ZGRHJxs/Captura-de-pantalla-de-2019-11-07-10-47-48.png)



Here is a image of the start of the execution

![main_launcher.png](https://i.ibb.co/C8dVPkm/Captura-de-pantalla-de-2019-11-07-10-48-15.png)



Here is a video of a correct execution of the project and mission:

[ ![Mission](https://img.youtube.com/vi/H86NqY-jhMA/0.jpg)](https://youtu.be/H86NqY-jhMA)


