#!/bin/bash

DRONE_SWARM_MEMBERS=$1
export AEROSTACK_PROJECT=${AEROSTACK_STACK}/projects_cvar/tower_inspection_rotors_simulator
if [ -z $DRONE_SWARM_MEMBERS ] # Check if NUMID_DRONE is NULL
  then
  	#Argument 1 empty
    	echo "-Setting Swarm Members = 2"
    	DRONE_SWARM_MEMBERS=2
  else
    	echo "-Setting DroneSwarm Members = $1"
fi

gnome-terminal  \
   	--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch ${AEROSTACK_PROJECT}/rotors_files/launch/Tower.launch --wait project:=${AEROSTACK_PROJECT};
						exec bash\""  &

 
gnome-terminal  \
   	--tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch ${AEROSTACK_PROJECT}/rotors_files/launch/mav_swarm.launch --wait drone_swarm_number:=1 mav_name:=hummingbird posx:=2 posy:=-7;
						exec bash\""  &


gnome-terminal  \
        --tab --title "DroneRotorsSimulator" --command "bash -c \"
roslaunch ${AEROSTACK_PROJECT}/rotors_files/launch/mav_swarm.launch --wait drone_swarm_number:=2 mav_name:=hummingbird posx:=1 posy:=-8; 
                                                exec bash\""  &


#gnome-terminal  \
 #       --tab --title "DroneRotorsSimulator" --command "bash -c \"
#roslaunch rotors_gazebo mav_swarm.launch --wait drone_swarm_number:=3 mav_name:=hummingbird posx:=0 posy:=-3; 
 #                                               exec bash\""  &









