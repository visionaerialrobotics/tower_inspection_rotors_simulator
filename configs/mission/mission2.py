#!/usr/bin/env python2

import executive_engine_api as api
import rospy
import time
import sys
'''
This is a simple mission, the drone takes off, follows a path and lands
There is a little detail in the way of activating the behaviors:
1.activateBehavior. Activates the behavior and let other behaviors run.
2.executeBehavior. Activates the behavior and do not let other behaviors run.
'''

#  GLOBAL
global floor


def runMission():
  global floor

  floor=1

  print("-----------------------------------------------")
  print("Starting mission...")

  print("-----------------------------------------------")
  api.activateBehavior('PAY_ATTENTION_TO_ROBOT_MESSAGES')
  print("Listening")

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("- Taking off...")
  api.executeBehavior('TAKE_OFF')
  floor=0

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  if floor==1: # when a robot checks the mesagges, has to be on the air
    print("Forcing to take of to follow path")
    api.executeBehavior('TAKE_OFF')
    floor=0

  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 1] ]) # low level out

  executeMessage()

  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 8, 1] ]) # low level in

  ##########################  ANALYSING THE LOW FLOOR ###############################

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("- Asking operator...")
  api.executeBehavior('REQUEST_OPERATOR_ASSISTANCE', question="Are there any problems on the low level?", belief_predicate_name="low_level", options=["low_level_ok", "low_level_error"])
  
  executeMessage()


  #######################################################################################


  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 1] ]) # low level out
  executeMessage()


  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Sending message...")
  api.executeBehavior('INFORM_ROBOTS', source= "drone112", destination="drone111", text="Drone112_going_to_the_next_floor")
  print("Next floor")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 6] ]) # first level out

  executeMessage()

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 8, 6] ]) # first level in

  ##########################  ANALYSING THE FIRST FLOOR  ###############################

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("- Asking operator...")
  api.executeBehavior('REQUEST_OPERATOR_ASSISTANCE', question="Are there any problems on the first floor?", belief_predicate_name="first_floor", options=["first_floor_ok", "first_floor_error"])
  
  executeMessage()

  # #######################################################################################

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 6] ]) # first level out

  executeMessage()

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Sending message...")
  api.executeBehavior('INFORM_ROBOTS', source= "drone112", destination="drone111", text="Drone112_going_to_the_next_floor")
  print("Next floor")
  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 10.4] ]) # second level out

  executeMessage()

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 8, 10.4] ]) # second level in

  executeMessage()

  # ##########################  ANALYSING THE SECOND FLOOR ###############################

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("- Asking operator...")
  api.executeBehavior('REQUEST_OPERATOR_ASSISTANCE', question="Are there any problems on the second floor?", belief_predicate_name="second_floor", options=["second_floor_ok", "second_floor_error"])
  
  executeMessage()

  # #######################################################################################


  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 10.4] ]) # second level out
  
  executeMessage()


  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Sending message...")
  api.executeBehavior('INFORM_ROBOTS', source= "drone112", destination="drone111", text="Drone112_going_to_the_next_floor")
  print("Next floor")
  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 14.9] ]) # third level out

  executeMessage()

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 8, 14.9] ]) # third level in

  executeMessage()

  # ##########################  ANALYSING THE THIRD FLOOR  ###############################

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("- Asking operator...")
  api.executeBehavior('REQUEST_OPERATOR_ASSISTANCE', question="Are there any problems on the third floor?", belief_predicate_name="third_floor", options=["third_floor_ok", "third_floor_error"])
  
  executeMessage()

  # #######################################################################################
  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Following path...")
  api.executeBehavior('FOLLOW_PATH', path=[ [-8.2, 3.5, 14.9] ]) # third level out

  executeMessage()

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("Sending message...")
  api.executeBehavior('INFORM_ROBOTS', source= "drone112", destination="drone111", text="Fin")
  print("Fin")

  executeMessage()

  print("-----------------------------------------------")
  print("Done with my own mission")
  print("-----------------------------------------------")

  justListen() 

  # messages=1
  # while messages:
  #   s, num = api.queryBelief('sender(?M,drone111)')
  #   success, message = api.queryBelief('text('+str(num['M'])+',?W)')
  #   if success:
  #     api.retractBelief('object('+str(num['M'])+',message)')
  #     api.retractBelief('sender('+str(num['M'])+',drone111)')
  #     api.retractBelief('text('+str(num['M'])+','+message['W']+')') 
  #   else:
  #     messages=0
  


#-------------FUNCION AUX1-------------#
#-------------Funcion para ejecutar todas las acciones propias-------------#
def executeMessage():
  global floor

  print("***********************************************")
  print("----------    Looking for messages   ----------")

  s, num = api.queryBelief('sender(?N,drone111)')
   
  if s==0: #does not wait
    print("-----------------------------------------------")
    print("No message")
    return
  else:
    success, message = api.queryBelief('text('+str(num['N'])+',?T)')
  
  while success: # reading all the messages
    print("-----------------------------------------------")
    print("Message found:")
    print(message['T'])

    if message['T']=="Done": 
        print("-----------------------------------------------")
        print("Finishing mission...")
        api.executeBehavior('LAND')
        api.retractBelief('object('+str(num['N'])+',message)')
        api.retractBelief('sender('+str(num['N'])+',drone112)')
        api.retractBelief('text('+str(num['N'])+','+message['T']+')')
        api.executeBehavior('INFORM_OPERATOR', message="Drone112: mission finished")
        keepListening=0
        success=0
        sys.exit()
           

    else: 
        api.retractBelief('object('+str(num['N'])+', message)')
        api.retractBelief('sender('+str(num['N'])+',drone111)')
        api.retractBelief('text('+str(num['N'])+','+message['T']+')')
        s, num = api.queryBelief('sender(?N,drone111)')
        if s==0:  # No more messages by this time  
            print("-----------------------------------------------")
            print("No more messages found")
            success=0
        else:
            success, message = api.queryBelief('text('+str(num['N'])+',?T)') 



#-------------FUNCION AUX2-------------#
#-------------Funcion para mirar mensajes-------------#
# Variables: 
# N = object id
# S = sender id
# T = text
# O = type of object

def justListen():

      global floor

      keepListening=1
      print("***********************************************")
      print("-------------    justListen   -----------------")
     
      while keepListening: # Listening until the sender tell us not to

        s, num = api.queryBelief('sender(?N,drone111)')
        while s==0:
          print("Waiting...")
          s, num = api.queryBelief('sender(?N,drone111)')

        success, message = api.queryBelief('text('+str(num['N'])+',?T)')
        print("-----------------------------------------------")
        print("First message:")
        print(message['T'])
        
        while success:

           if message['T']=="Done":
            print("-----------------------------------------------")
            print("Finishing mission...")
            api.executeBehavior('LAND')
            api.retractBelief('object('+str(num['N'])+',message)')
            api.retractBelief('sender('+str(num['N'])+',drone111)')
            api.retractBelief('text('+str(num['N'])+','+message['T']+')')
            api.executeBehavior('INFORM_OPERATOR', message="Drone112: mission finished")
            keepListening=0
            success=0
           

           else: 
            print("-----------------------------------------------")
            print("Not done:")
            print(message['T'])
            api.retractBelief('object('+str(num['N'])+', message)')
            api.retractBelief('sender('+str(num['N'])+',drone111)')
            api.retractBelief('text('+str(num['N'])+','+message['T']+')')
            s, num = api.queryBelief('sender(?N,drone111)')
            while s==0:
             print("Waiting...")
             s, num = api.queryBelief('sender(?N,drone111)')
            success, message = api.queryBelief('text('+str(num['N'])+',?T)')