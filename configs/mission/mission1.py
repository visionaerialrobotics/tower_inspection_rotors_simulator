#!/usr/bin/env python2

import executive_engine_api as api
import rospy
import sys
'''
This is a simple mission, the drone takes off, follows a path and lands
There is a little detail in the way of activating the behaviors:
1.activateBehavior. Activates the behavior and let other behaviors run.
2.executeBehavior. Activates the behavior and do not let other behaviors run.
'''

#  GLOBAL
#  1=yes    0=no
global floor
global level
global out
global keepListening


def runMission():
  global floor
  global level
  global out

  floor=1
  keepListening=1


  print("-----------------------------------------------")
  print("Starting mission...")

  print("-----------------------------------------------")
  api.activateBehavior('PAY_ATTENTION_TO_ROBOT_MESSAGES')
  print("Listening")

  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("- Taking off...")
  api.executeBehavior('TAKE_OFF')
  floor=0
  
  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  if floor==1: # when a robot checks the mesagges, has to be on the air
    print("Forcing to take of to follow path")
    api.executeBehavior('TAKE_OFF')
    floor=0
  print("Following path...")
  out=1
  api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 0.7] ]) # low level out
  level=0

  while keepListening:
    if level==0:
      location()
      lowLevel()
      executeMessage()
    elif level==1: 
      location()
      firstFloor() 
      executeMessage()
    elif level==2: 
      location()
      secondFloor()
      executeMessage() 
    elif level==3: 
      location()
      thirdFloor() 
      executeMessage()


#-------------FUNCION AUX-------------#
#-------------Funcion para ejecutar todas las acciones propias-------------#
def executeMessage():
  global floor
  global level
  global out
  global keepListening


  print("***********************************************")
  print("----------    Looking for messages   ----------")

  s, num = api.queryBelief('sender(?N,drone112)')
   
  while s==0: # waits to know if it has to get in the building or not
    print("Waiting...")
    s, num = api.queryBelief('sender(?N,drone112)')
  success, message = api.queryBelief('text('+str(num['N'])+',?T)')
  
  while success:
    print("-----------------------------------------------")
    print("Message found:")
    print(message['T'])
    if message['T']=="Drone112_going_to_the_next_floor":
      api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_will_follow_you")
      print("-----------------------------------------------")
      if level==0:
        if out==0:
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 0.7] ]) # low level out
          out=1
        api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 5.7] ]) # first level out
      elif level==1:
        if out==0:
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 5.7] ]) # first level out
          out=1
        api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 10.1] ]) # second level out
      elif level==2:
        if out==0:
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 10.1] ]) # second level out
          out=1
        api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 14.6] ]) # third level out
      else:
        if out==0:
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 1.5, 14.6] ]) # third level out
          out=1

      level=level+1

      api.retractBelief('object('+str(num['N'])+',message)')
      api.retractBelief('sender('+str(num['N'])+',drone112)')
      api.retractBelief('text('+str(num['N'])+','+message['T']+')')
      success=0

    elif message['T']=="Fin":
      print("-----------------------------------------------")
      print("Finishing mission...")
      api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="Done")
      api.executeBehavior('LAND')
      api.retractBelief('object('+str(num['N'])+',message)')
      api.retractBelief('sender('+str(num['N'])+',drone112)')
      api.retractBelief('text('+str(num['N'])+','+message['T']+')')
      api.executeBehavior('INFORM_OPERATOR', message="Drone111: mission finished")
      keepListening=0
      success=0
      sys.exit()

    else:
      print("-----------------------------------------------")
      print("Message not allowed:")
      print(message['T'])
      api.retractBelief('object('+str(num['N'])+',message)')
      api.retractBelief('sender('+str(num['N'])+',drone112)')
      api.retractBelief('text('+str(num['N'])+','+message['T']+')')
      s, num = api.queryBelief('sender(?N,drone112)')
      while s==0:
        print("Waiting...")
        s, num = api.queryBelief('sender(?N,drone112)')
      success, message = api.queryBelief('text('+str(num['N'])+',?T)')


#-------------FUNCION AUX0-------------#
#-------------Funcion para enviar localizacion-------------# 
def location():
  global level
  print("-----------------------------------------------")
  print("-------------    own action   -----------------")
  print("-----------------------------------------------")
  print("Sending message...")
  if level==0 and out==1:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_outside_the_low_level")
  elif level==0 and out==0:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_inside_the_low_level")
  elif level==1 and out==1:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_outside_the_first_floor")
  elif level==1 and out==0:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_inside_the_first_floor")
  elif level==2 and out==1:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_outside_the_second_floor")
  elif level==2 and out==0:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_inside_the_second_floor")
  elif level==3 and out==1:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_outside_the_third_floor")
  elif level==3 and out==0:
    api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="I_am_inside_the_third_floor")
  print("Sending location")


#-------------FUNCION AUX1-------------#
#-------------Funcion para inspeccionar planta baja-------------#
def lowLevel():
  global floor
  global level
  global out
  global keepListening

  print("***********************************************")
  print("----------    Looking for messages   ----------")

  success, message = api.queryBelief('low_level(?N)')
   
  while success==0:
    print("Waiting...")
    success, message = api.queryBelief('low_level(?N)')
  
  if success: # reading all the messages

    print("-----------------------------------------------")
    print("Message found:")
    print(message['N'])

    if message['N']=="low_level_ok":
        print("-----------------------------------------------")
        print("Executing...")
        api.retractBelief('low_level(low_level_ok)')

    elif message['N']=="low_level_error":
        if out==1: 
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 7, 0.7] ]) #low level in
          out=0
        print("-----------------------------------------------")
        print("Executing...")
        api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="Problem_in_the_low_level_solved")
        api.retractBelief('low_level(low_level_error)')


#-------------FUNCION AUX1-------------#
#-------------Funcion para inspeccionar planta baja-------------#
def firstFloor():
  global floor
  global level
  global out
  global keepListening

  print("***********************************************")
  print("----------    Looking for messages   ----------")

  success, message = api.queryBelief('first_floor(?N)')
   
  while success==0:
    print("Waiting...")
    success, message = api.queryBelief('first_floor(?N)')
  
  if success: # reading all the messages

    print("-----------------------------------------------")
    print("Message found:")
    print(message['N'])

    if message['N']=="first_floor_ok":
        print("-----------------------------------------------")
        print("Executing...")
        api.retractBelief('first_floor(first_floor_ok)')

    elif message['N']=="first_floor_error":
        if out==1: 
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 7, 5.7] ]) #first floor in
          out=0
        print("-----------------------------------------------")
        print("Executing...")
        api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="Problem_in_the_first_floor_solved")
        api.retractBelief('first_floor(first_floor_error)')




#-------------FUNCION AUX1-------------#
#-------------Funcion para inspeccionar planta baja-------------#
def secondFloor():
  global floor
  global level
  global out
  global keepListening

  print("***********************************************")
  print("----------    Looking for messages   ----------")

  success, message = api.queryBelief('second_floor(?N)')
   
  while success==0:
    print("Waiting...")
    success, message = api.queryBelief('second_floor(?N)')
  
  if success: # reading all the messages

    print("-----------------------------------------------")
    print("Message found:")
    print(message['N'])

    if message['N']=="second_floor_ok":
        print("-----------------------------------------------")
        print("Executing...")
        api.retractBelief('second_floor(second_floor_ok)')

    elif message['N']=="second_floor_error":
        if out==1: 
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 7, 10.1] ]) #second floor in
          out=0
        print("-----------------------------------------------")
        print("Executing...")
        api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="Problem_in_the_second_floor_solved")
        api.retractBelief('second_floor(second_floor_error)')


#-------------FUNCION AUX1-------------#
#-------------Funcion para inspeccionar planta baja-------------#
def thirdFloor():
  global floor
  global level
  global out
  global keepListening

  print("***********************************************")
  print("----------    Looking for messages   ----------")

  success, message = api.queryBelief('third_floor(?N)')
   
  while success==0:
    print("Waiting...")
    success, message = api.queryBelief('third_floor(?N)')
  
  if success: # reading all the messages

    print("-----------------------------------------------")
    print("Message found:")
    print(message['N'])

    if message['N']=="third_floor_ok":
        print("-----------------------------------------------")
        print("Executing...")
        api.retractBelief('third_floor(third_floor_ok)')

    elif message['N']=="third_floor_error":
        if out==1: 
          api.executeBehavior('FOLLOW_PATH', path=[ [-9.2, 7, 14.6] ]) #third floor in
          out=0
        print("-----------------------------------------------")
        print("Executing...")
        api.executeBehavior('INFORM_ROBOTS', source= "drone111", destination="drone112", text="Problem_in_the_third_floor_solved")
        api.retractBelief('third_floor(third_floor_error)')